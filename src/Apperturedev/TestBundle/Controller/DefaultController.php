<?php

namespace Apperturedev\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Apperturedev\CouchbaseBundle\Classes\CouchbaseORM;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\Serializer;
use Apperturedev\TestBundle\Entity\Test;

class DefaultController extends Controller
{
    /**
     *
     * @param Serializer $serializer
     * @return JsonResponse
     */
    public function indexAction() {
        $serializer = $this->getSerializer();
        $couchbase = $this->getCouchabase();
        // $test = new Test();
        // $test->setName('fred');
        // $test->setUsername('fredpalas');
        // $test->setNumber(123456);
        // $couchbase->save($test);
        $test = $couchbase->getReposity('TestBundle:Test')->getById(8);
        $doctrine = $this->getDoctrine();
        $doctrine->getManager();
        //
        //
        $array = $serializer->toArray($test);

        return new JsonResponse($array);
    }

    /**
     *
     * @return CouchbaseORM
     */
    private function getCouchabase(){
        return $this->get('couchbase');
    }

    /**
     *
     * @return Serializer
     */
    private function  getSerializer(){
        return $this->get('jms_serializer');
    }
}
